# use 'e' for epsilon
# firsts and follows will be stored to firsts.txt and follows.txt respectively
# the result of checking for ll(1) will be shown in the terminal

from collections import OrderedDict

rules = []
firsts = []

rules_dict = OrderedDict()
firsts_dict = OrderedDict()
follow_dict = OrderedDict()


def non_terminal_appender(firsts, rules):
    for rule in rules:
        if rule[0][0] not in firsts:
            firsts.append(rule[0][0])
            firsts_dict[rule[0][0]] = []
            follow_dict[rule[0][0]] = []

with open("rules_test.txt", "r") as fp:
    for line in fp:
        rules.append(line.strip().split('\n'))


def add_first(rule, index):
    if not rule[0][index].isupper():
        firsts_dict[rule[0][0]].extend(rule[0][index])
    else:
        firsts_dict[rule[0][0]].extend(firsts_dict[rule[0][index]])
        if 'e' in firsts_dict[rule[0][index]]:
            if index == len(rule[0]) - 1:
                firsts_dict[rule[0][0]].extend('e')
            else:
                add_first(rule, index + 1)


def add_follow(current_non_term_index):
    if current_non_term_index == (len(tmp_rule_list) - 1):
        if k != rules_keys[i]:
            follow_dict[rules_keys[i]].extend(follow_dict[k])
    else:
        if not tmp_rule_list[current_non_term_index + 1].isupper():
            follow_dict[rules_keys[i]].extend(tmp_rule_list[current_non_term_index + 1])
        else:
            follow_dict[rules_keys[i]].extend(firsts_dict[tmp_rule_list[current_non_term_index + 1]])
            if 'e' in firsts_dict[tmp_rule_list[current_non_term_index + 1]]:
                add_follow(current_non_term_index + 1)

# ++++++    Following code is used to find firsts   +++++++++
number_of_rules = len(rules)
rule_count = first_count = 0
non_terminal_appender(firsts, rules)
for rule in rules:
    rules_dict[rule[0][0]] = []
for rule in rules:
    rules_dict[rule[0][0]].append(rule[0][3:])

for rule in rules:
    if not rule[0][3].isupper():
        firsts_dict[rule[0][0]].extend(rule[0][3])
for rule in rules:
    add_first(rule, 3)
for rule in rules:
    add_first(rule, 3)

for i in firsts_dict:
    firsts_dict[i] = list(set(firsts_dict[i]))

with open("firsts.txt", "w+") as wp:
    for k in firsts_dict:
        wp.write("first(%s): \t " % k)
        wp.write("%s\n" % firsts_dict[k])

# ++++++    Following code is used to find the follows  ++++++
rules_keys = rules_dict.keys()
key_count = len(rules_keys)

for k in rules_dict:
    tmp_rule = rules_dict[k]
    for tmp_rule_str in tmp_rule:
        if k == rules_keys[0]:
            follow_dict[k].append('$')
        for i in xrange(key_count):
            if rules_keys[i] in tmp_rule_str:
                tmp_rule_list = list(tmp_rule_str)
                current_non_term_index = tmp_rule_list.index(rules_keys[i])

                add_follow(current_non_term_index)

for k in rules_dict:
    tmp_rule = rules_dict[k]
    for tmp_rule_str in tmp_rule:
        if k == rules_keys[0]:
            follow_dict[k].append('$')
        for i in xrange(key_count):
            if rules_keys[i] in tmp_rule_str:
                tmp_rule_list = list(tmp_rule_str)
                current_non_term_index = tmp_rule_list.index(rules_keys[i])

                add_follow(current_non_term_index)

for i in follow_dict:
    follow_dict[i] = list(set(follow_dict[i]))
    if 'e' in follow_dict[i]:
        follow_dict[i].remove('e')

with open("follows.txt", "w+") as wp:
    for k in follow_dict:
        wp.write("follow(%s): \t" % k)
        wp.write("%s\n" % follow_dict[k])


def duplicate(left_char, rules):
    if 'e' in rules:
        rules.remove('e')
        for i in xrange(len(rules)):
            if not rules[i][0].isupper():
                if rules[i][0] in follow_dict[left_char]:
                    return True
            else:
                for l in firsts_dict[rules[i][0]]:
                    if l in follow_dict[left_char]:
                        return True

    for i in xrange(len(rules)):
        for j in range(i + 1, len(rules)):
            if not rules[i][0].isupper():
                if not rules[j][0].isupper():
                    if rules[i][0] == rules[j][0]:
                        return True
                else:
                    if rules[i][0] in firsts_dict[rules[j][0]]:
                        return True
            else:
                if not rules[j][0].isupper():
                    if rules[j][0] in firsts_dict[rules[i][0]]:
                        return True
                else:
                    for l in firsts_dict[rules[i][0]]:
                        if l in firsts_dict[rules[j][0]]:
                            return True

ll1 = 1

for rule in rules_dict:
    if len(rules_dict[rule]) > 1:
        if duplicate(rule, rules_dict[rule]):
            ll1 = 0

if ll1:
    print 'the given grammar is ll(1)'
else:
    print 'the given grammar is NOT ll(1)'
